{ lib, hyraizyn, krimynNeim, ... }:
let
  inherit (builtins) attrNames;
  inherit (lib) mkOption;
  inherit (lib.types) enum str;

  krimynNeimz = attrNames hyraizyn.krimynz;

in
{
  options = {
    krimynNeim = mkOption {
      type = enum krimynNeimz;
    };
  };

  config = {
    home = {
      username = krimynNeim;
      homeDirectory = "/home/" + krimynNeim;
    };
  };
}
