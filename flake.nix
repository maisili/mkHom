{
  description = "mkHom";

  outputs = { self, mkIval, gluHomeManager, uyrld }@fleiks: {
    strok = {
      djenyreicyn = 10;
      spici = "lamdy";
    };

    datom = { hyraizyn, krimynNeim }:
    let
      beisModule = import ./beisModule.nix;

      modules = [ beisModule ] ++ gluHomeManager.datom.modules;

      ival = mkIval.datom { inherit hyraizyn; };

      bildHom = darkOrLait: (ival {
        inherit modules;
        moduleArgz = { inherit krimynNeim darkOrLait; };
      }).config.home.activationPackage;

      darkLaitBildz = {
        dark = bildHom "dark";
        lait = bildHom "lait";
      };

      bildHomSkrip = uyrld.datom.ruskrip {
        neim = "bildHom";
        skrip = builtins.readFile ./bildHom.rs;
        argz = {
          inherit darkLaitBildz;
        };
      };

    in bildHomSkrip;

  };
}
